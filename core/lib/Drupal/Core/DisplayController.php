<?php

/**
 * @file
 * Definition of Drupal\Core\Booze\DrunkController.
 */

namespace Drupal\Core;

use Drupal\Core\FragmentRenderer\BlockFragmentRendererInterface;
use Drupal\block\Plugin\Type\BlockManager;
use Drupal\Core\Plugin\Context\ContextResolver;
use Drupal\layout\Config\UnboundDisplayInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\layout\Config\BoundDisplayInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\system\Plugin\Block\SystemMainBlock;
use Drupal\Core\Page\HtmlFragment;
use Symfony\Component\HttpKernel\Fragment\FragmentRendererInterface;

/**
 * Default controller for handling text/html responses.
 *
 * This is the base controller for the "blocks and layouts" rendering system,
 * specifically the respond() method. That takes a
 * Drupal\layout\Config\BoundDisplayInterface object, which contains a set of
 * blocks and a layout plugin, and renders them into a PageFragment object,
 * itself a renderable object that is used by later parts of the system.
 */
class DisplayController {

  /**
   * An array containing rendered block output, keyed by the block instance that
   * produced it.
   *
   * @var array
   */
  public $blockFragments = array();

  /**
   * An associative array of rendered regions.
   * @var array
   */
  public $renderedRegions;

  /**
   * The Response object that is built up over the course of the request and
   * eventually returned from this controller.
   *
   * @var \Symfony\Component\HttpFoundation\Response
   */
  protected $response;

  /**
   * The display configuration object containing layout and block information,
   * used by this controller to assemble all page elements.
   *
   * @var \Drupal\layout\Config\BoundDisplayInterface
   */
  protected $display;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @var \Drupal\block\Plugin\Type\BlockManager
   */
  protected $blockManager;

  /**
   * @var \Drupal\Core\Plugin\Context\ContextResolver
   */
  protected $resolver;

  /**
   * An array of FragmentRendererInterface objects.
   *
   * @var array
   */
  protected $renderingStrategies = array();

  /**
   * Constructs the DisplayController service.
   *
   * @param ModuleHandlerInterface $moduleHandler
   *   The module handler service, used for hook invocations.
   * @param BlockManager           $blockManager
   *   The BlockManager, used to instantiate blocks.
   */
  public function __construct(ModuleHandlerInterface $moduleHandler, BlockManager $blockManager, ContextResolver $resolver) {
    $this->moduleHandler = $moduleHandler;
    $this->blockManager = $blockManager;
    $this->resolver = $resolver;
  }

  /**
   * Adds a block rendering strategy.
   *
   * @todo probably best to shift this into a plugin, then inject the manager.
   *
   * @param BlockFragmentRendererInterface $strategy
   *   A FragmentRendererInterface instance
   */
  public function addRenderer(BlockFragmentRendererInterface $strategy) {
    $this->renderingStrategies[$strategy->getName()] = $strategy;
  }

  /**
   * Controller method for normal rendering of blocks and layouts-driven HTML
   * page.
   *
   * @todo the output may need more or less assembling, depending on how
   * 'compiled' of a template we have in the display. We either can handle it
   * below here via a delegator pattern, or with sibling classes.
   *
   * @param \Symfony\Component\HttpFoundation\Request         $request
   *   The request object.
   * @param \Drupal\layout\Config\BoundDisplayInterface       $_display
   *   A configuration object containing the layout instance and set of
   *   block instances that have been configured to be used for the current
   *   route.
   *
   * @return \Drupal\Core\Page\HtmlFragment
   */
  public function respond(Request $request, BoundDisplayInterface $_display) {
    $this->display = $_display;

    // Create a HtmlFragment object right away that we can easily decorate as we go.
    $this->response = new HtmlFragment();

    $this->blockFragments = $this->renderBlocks($request, $this->display);
    $this->renderRegions($request);

    $content = $this->display->getLayoutInstance()->renderLayout(FALSE, $this->renderedRegions);

    $this->response->setContent($content);

    return $this->response;
  }

  /**
   * Renders all blocks contained in the provided display.
   *
   * @param Request               $request
   *   The current Request object.
   * @param BoundDisplayInterface $display
   *   The Display to render.
   *
   * @return array
   *   An array of HtmlFragment objects, returned from block rendering.
   */
  protected function renderBlocks(Request $request, BoundDisplayInterface $display) {
    $blockFragments = array();
    // Resolve the contexts in the display.
    $resolutions = $this->resolver->resolveContexts($request, $display->getContextConfigurations());
    foreach ($display->getAllOuterBlockConfig() as $id => $config) {
      $block = $display->getBlock($id);
      // Check for any expected contexts for this block.
      $definition = $block->getContextDefinitions();
      // If there are expected contexts, and the display is providing a map to
      // contexts it has available, set the context values for the block.
      if ($definition && isset($config['contexts'])) {
        foreach ($config['contexts'] as $local_context_slot => $display_context_id) {
          if (isset($resolutions[$display_context_id])) {
            $block->setContextValue($local_context_slot, $resolutions[$display_context_id]);
          }
        }
      }

      if ($block instanceof SystemMainBlock) {
        $block->setControllerClosure($request->attributes->get('_content_closure'));
      }

      $munged_request = clone $request;
      // @todo muck with the Request here to get it to where we need
      $blockFragments[$id] = $this->renderingStrategies[$config['strategy']]->render($block, $munged_request);
    }

    return $blockFragments;
  }

  /**
   * Renders all rendered blocks into their respective layout regions, using
   * the layout attached to the current display object.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   */
  protected function renderRegions(Request $request) {
    $layout = $this->display->getLayoutInstance();
    $outer_configs = $this->display->getAllOuterBlockConfig();
    $group_configs = $this->display->getAllGroupConfig();

    // @todo this entire method is a mess. trim it all down by hiding the munging logic in iterators and bags

    foreach ($layout->getRegions() as $region => $info) {
      $groups = $rendered = array();

      foreach ($this->display->getSortedBlocksByRegion($region) as $id) {
        $block_weight = $outer_configs[$id]['weight'];

        if (!empty($outer_configs[$id]['group'])) {
          $group_name = $outer_configs[$id]['group'];
          if (!isset($group_configs[$group_name])) {
            throw new \Exception(sprintf('Block "%s" indicates it is part of group "%s", but that group does not exist.', $id, $group_name), E_RECOVERABLE_ERROR);
          }

          while (!empty($groups[$group_name][$block_weight])) {
            $block_weight += 0.001;
          }
          $groups[$group_name][$block_weight] = $this->blockFragments[$id]->getContent();
        }
        else {
           while (!empty($rendered[$block_weight])) {
             $block_weight += 0.001;
           }
           $rendered[$block_weight] = $this->blockFragments[$id]->getContent();
        }
      }

      foreach ($groups as $group_name => $blocks) {
        $group = array(
          '#theme' => 'group',
          '#blocks' => implode("", $blocks),
          '#config' => $group_configs[$group_name],
        );
        $rendered[$group_configs[$group_name]['weight']] = drupal_render($group);
      }

      ksort($rendered);
      $this->renderedRegions[$region] = implode("", array_filter($rendered));
    }
  }
}
