<?php

/**
 * @file
 * Contains \Drupal\Core\FragmentRenderer\FragmentHandler.
 */
namespace Drupal\Core\FragmentRenderer;

use Drupal\Core\Page\HtmlFragment;
use Symfony\Component\HttpKernel\Fragment\FragmentHandler as BaseFragmentHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Fragment\FragmentRendererInterface;

/**
 * Renders a block into an HtmlFragment using a particular rendering strategy.
 *
 * This class proxies requests to render a block as an HtmlFragment off to the
 * appropriate named FragmentRendererInterface service, where the real work is
 * done.
 *
 * @see BlockFragmentRendererInterface
 */
class FragmentHandler extends BaseFragmentHandler {
  private $debug;
  private $renderers;
  private $requests;

  /**
   * Sets up a BlockFragmentRenderer service.
   *
   * @param array $renderers
   *   An array of FragmentRendererInterface instances
   * @param Boolean $debug
   *   Whether the debug mode is enabled or not
   */
  public function __construct(array $renderers = array(), $debug = FALSE) {
    $this->renderers = array();
    foreach ($renderers as $renderer) {
      $this->addRenderer($renderer);
    }
    $this->debug = $debug;
    $this->requests = array();
  }

  /**
   * Renders and returns an HtmlFragment from a block.
   *
   * @param \Drupal\block\BlockInterface $block
   *   The block to be rendered.
   * @param string $renderer
   *   The name of the BlockFragmentRendererInterface to use.
   * @param array $options
   *   An array of options to pass to the renderer.
   *
   * @return HtmlFragment|Response
   *   An HtmlFragment object representing the block's output and accompanying
   *   out-of-band information (primarily title and assets).
   *
   * @throws \InvalidArgumentException
   *   Thrown if the renderer does not exist.
   */
  public function render($block, $renderer = 'block_inline', array $options = array()) {
    if (!isset($options['ignore_errors'])) {
      $options['ignore_errors'] = !$this->debug;
    }

    if (!isset($this->renderers[$renderer])) {
      throw new \InvalidArgumentException(sprintf('The "%s" fragment renderer does not exist.', $renderer));
    }

    // Support both native Symfony and Drupal fragment rendering patterns.
    // @todo this is kinda horrible.
    if ($this->renderers[$renderer] instanceof BlockFragmentRendererInterface) {
      return $this->renderers[$renderer]->render($block, $this->requests[0], $options);
    }
    else if ($this->renderers[$renderer] instanceof FragmentRendererInterface) {
      return $this->deliver($this->renderers[$renderer]->render($block, $this->requests[0], $options));
    }
  }
}
