<?php

/**
 * @file
 * Contains Drupal\Core\FragmentRenderer\BlockFragmentRendererInterface.
 */

namespace Drupal\Core\FragmentRenderer;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Fragment\FragmentRendererInterface;

/**
 * Interface for all block rendering strategies.
 *
 * @todo eeeew. playing interface games like this is terrible.
 */
interface BlockFragmentRendererInterface extends FragmentRendererInterface {
  /**
   * Renders a block, returning an HtmlFragment.
   *
   * This supercedes the render method of the parent interface by specifying
   * that an HtmlFragment be returned.
   *
   * @todo is it best to pass the block plugin in here, or something else?
   * @param \Drupal\block\BlockInterface $block
   *   A block plugin, fully injected with its configuration and data.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A Request instance.
   * @param array $options
   *   An array of options to be used in rendering the fragment.
   *
   * @return \Drupal\Core\Page\HtmlFragment
   */
  public function render($block, Request $request, array $options = array());
}