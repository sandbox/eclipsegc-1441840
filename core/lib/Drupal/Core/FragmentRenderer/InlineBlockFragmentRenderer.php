<?php

/**
 * @file
 * Contains \Drupal\Core\FragmentRenderer\InlineBlockFragmentRenderer.
 */

namespace Drupal\Core\FragmentRenderer;

use Drupal\Core\Page\HtmlFragment;
use Drupal\block\BlockAddressGenerator;
use Drupal\block\BlockControllerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\block\BlockPluginInterface;
use Symfony\Component\HttpKernel\Fragment\InlineFragmentRenderer;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Block fragment renderer for inline placement of final block output.
 *
 * This strategy is sensitive to whether or not a block has indicated that it is
 * subrequest safe. If it is, then a subrequest is used; otherwise, the block is
 * directly rendered here via the block rendering service here.
 */
class InlineBlockFragmentRenderer extends InlineFragmentRenderer implements BlockFragmentRendererInterface {
  /**
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $kernel;

  /**
   * The BlockAddressGenerator service to use.
   *
   * @var \Drupal\block\BlockAddressGenerator
   */
  protected $blockAddressor;

  /**
   * @var \Drupal\block\BlockControllerInterface
   */
  protected $blockController;

  public function __construct(HttpKernelInterface $kernel, BlockAddressGenerator $blockAddressor, BlockControllerInterface $blockController) {
    $this->kernel = $kernel;
    $this->blockAddressor = $blockAddressor;
    $this->blockController = $blockController;
  }

  /**
   * Gets the name of the strategy.
   *
   * @return string
   *   The system name of the strategy.
   */
  public function getName() {
    return 'inline_block';
  }

  /**
   * Renders a block, returning an HtmlFragment.
   *
   * @todo is it best to pass the block plugin in here, or something else?
   *
   * @param \Drupal\block\BlockPluginInterface $block
   *   A block plugin, fully injected with its configuration and data.
   * @param Request                    $request A Request instance
   * @param array                      $options An array of options
   *
   * @return HtmlFragment
   */
  public function render($block, Request $request, array $options = array()) {
    if (!$block instanceof BlockPluginInterface) {
      throw new \InvalidArgumentException('A block instance must be provided to the fragment renderer.');
    }

    // @todo this needs to be rolled into the more general pattern of context prep
    if (!$block->isSafelyAddressable()) {
      // Have to check access first, since normally the subrequest would do it
      // for us.
      if ($block->access()) {
        $fragment = $this->blockController->respond($request, $block);
      }
      else {
        $fragment = new HtmlFragment();
        // @todo this needs to be brought into line with how the subrequest would handle it. ...which is a Response set with a 403. yikes.
      }
      return $fragment;
    }
    else {
      $display = empty($options['display']) ? '' : $options['display'];
      $subrequest = $this->createSubRequest($this->blockAddressor->generateAddress($block, $request, $display), $request);

      $level = ob_get_level();
      try {
        return $this->kernel->handle($subrequest, HttpKernelInterface::SUB_REQUEST, FALSE);
      } catch (\Exception $e) {
        // let's clean up the output buffers that were created by the sub-request
        while (ob_get_level() > $level) {
            ob_get_clean();
        }

        if (isset($options['alt'])) {
            $alt = $options['alt'];
            unset($options['alt']);

            return $this->render($alt, $request, $options);
        }

        if (!isset($options['ignore_errors']) || !$options['ignore_errors']) {
            throw $e;
        }

        // @todo decide what to return with if an exception is thrown in the subrequest
      }
    }
  }
}
