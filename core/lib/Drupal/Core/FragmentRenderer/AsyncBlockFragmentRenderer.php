<?php
/**
 * @file
 * Contains \Drupal\Core\FragmentRenderer\AsyncBlockFragmentRenderer.
 */
namespace Drupal\Core\FragmentRenderer;

use Symfony\Component\HttpKernel\Fragment\FragmentRendererInterface;

abstract class AsyncBlockFragmentRenderer implements FragmentRendererInterface {
  protected function getPathFromBlock($block) {
    // 1. determine differentiating vectors - config (instance), injected context
    // 2. determine injected services (yowza)
    // 3. extract URL
  }
}