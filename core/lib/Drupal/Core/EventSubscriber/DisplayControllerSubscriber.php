<?php

/**
 * @file
 * Definition of Drupal\Core\EventSubscriber\DrunkControllerSubscriber.
 */

namespace Drupal\Core\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Drupal\layout\Config\BoundDisplayInterface;

/**
 * Controller injector for text/html responses.
 */
class DisplayControllerSubscriber implements EventSubscriberInterface {

  /**
   * Attaches a display for blocks-driven HTML rendering, if appropriate.
   *
   * This determines if the Request and Route are appropriate candidates for
   * passing through block-driven rendering, and if so, attaches a controller
   * to do so.
   *
   * This is more or less the default case; we only don't take over if Drupal
   * is doing things like form processing, returning JSON or other things that
   * (generally) do not involve composing HTML.
   *
   */
  public function onDisplayControllerSetDisplay(FilterControllerEvent $event) {
    $request = $event->getRequest();

    // @todo implement a separate flag and remove this hardmapping
    if ('display_controller:respond' !== $request->attributes->get('_controller')
        || $request->attributes->get('_display') instanceof BoundDisplayInterface) {
      return;
    }

    $route = $request->attributes->get('_route');

    if (!empty($route)) {
      $display = entity_load('display', $route);
    }

    if (empty($display)) {
       throw new NotFoundHttpException(sprintf('No display could be located for the route %s.', $route));
       return;
    }

    $display->setMainContent($request->attributes->get('_content'));

    $request->attributes->set('_display', $display);
  }

  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::CONTROLLER][] = array('onDisplayControllerSetDisplay', 100);

    return $events;
  }
}