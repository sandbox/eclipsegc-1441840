<?php
/**
 * @file
 * Contains Drupal\Core\Plugin\ConfigEntityAwarePluginInterface.
 */

namespace Drupal\Core\Plugin;

use Drupal\Core\Config\Entity\EmbeddedPluginConfigInterface;

/**
 * Interface for plugins that use ConfigEntity for their configuration storage.
 */
interface ConfigEntityAwarePluginInterface {
  /**
   * Sets the ConfigEntity for this plugin.
   *
   * @param EmbeddedPluginConfigInterface $config
   *   The ConfigEntity object.
   *
   * @return void
   */
  public function setConfig(EmbeddedPluginConfigInterface $config);

  /**
   * Returns the ConfigEntity for this plugin.
   *
   * @return EmbeddedPluginConfigInterface
   */
  public function getConfig();

  /**
   * Returns the string identifier of the instance id.
   *
   * This is typically, though not necessarily, the full name of the
   * configuration object that backs the plugin's ConfigEntity.
   *
   * @return string
   */
  public function getInstanceId();
}