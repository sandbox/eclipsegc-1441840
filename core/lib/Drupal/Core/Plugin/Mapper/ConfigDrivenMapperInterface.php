<?php
/**
 * @file
 * Contains Drupal\Core\Plugin\Mapper\ConfigDrivenMapperInterface.
 */

namespace Drupal\Core\Plugin\Mapper;

use Drupal\Component\Plugin\Exception\PluginException;

/**
 * Defines an interface for retrieving plugins via a config id.
 *
 * For plugins that use the config system for their configuration CRUD, most
 * calling code will have only a config id, not a plugin id. The plugin id is
 * only stored in the configuration referenced by the config id.
 *
 * The most useful interface pattern for these cases is to simply allow calling
 * code to load the plugin by providing the config id.
 */
interface ConfigDrivenMapperInterface {
  /**
   * Gets a configured plugin instance from a configuration id.
   *
   * The calling code may optionally also specify a plugin id
   *
   * @param string $config_id
   *   The id of the configuration to load.
   *
   * @param string $plugin_id
   *   Optional. If provided, this plugin id will be used instead of whatever is
   *   specified by the loaded configuration.
   *
   * @return object
   *
   * @throws \InvalidArgumentException
   */
  public function getInstanceViaConfig($config_id, $plugin_id = '');
}