<?php

/**
 * @file
 * Contains \Drupal\Core\Plugin\Context\ContextResolver.
 */

namespace Drupal\Core\Plugin\Context;

use Drupal\Core\TypedData\TypedDataManager;
use Symfony\Component\HttpFoundation\Request;

/**
 * A class which resolves context configurations.
 */
class ContextResolver {

  /**
   * The typed data manager for resolving contexts.
   *
   * @var \Drupal\Core\TypedData\TypedDataManager
   */
  protected $manager;

  /**
   * @param TypedDataManager $manager
   */
  public function __construct(TypedDataManager $manager) {
    $this->manager = $manager;
  }

  /**
   * Resolves all the provided context configurations.
   *
   * @param Request $request
   * @param array $contexts
   * @return array
   */
  public function resolveContexts(Request $request, array $contexts) {
    $resolutions = array();
    foreach ($contexts as $id => $configuration) {
      switch ($configuration['contextType']) {
        case 'static':
          $resolutions[$id] = $this->staticContexts($configuration);
          break;
        case 'argument':
          // Extract context from the request
          break;
        case 'relationship':
          // Extract context from other contexts.
          break;
      }
    }
    return $resolutions;
  }

  /**
   * Resolve static context configurations.
   *
   * @param array $configuration
   * @return mixed
   */
  protected function staticContexts(array $configuration) {
    return $this->manager->create($configuration['definition'], $configuration['value'])->getValue();
  }
}
