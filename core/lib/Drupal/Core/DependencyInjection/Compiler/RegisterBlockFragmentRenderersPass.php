<?php

/**
 * @file
 * Contains Drupal\Core\DependencyInjection\Compiler\RegisterBlockFragmentRenderersPass.
 */

namespace Drupal\Core\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

/**
 * Adds services tagged 'render_strategy' to the 'fragment_handler' service.
 *
 * Drupal's block fragment rendering strategies utilize a slightly modified
 * interface vs. core symfony's, but our service can support either interface.
 */
class RegisterBlockFragmentRenderersPass implements CompilerPassInterface {
  /**
   * Adds services tagged 'render_strategy' to 'fragment_handler' services.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
   *  The container to process.
   */
  public function process(ContainerBuilder $container) {
    $services = array();
    foreach ($container->findTaggedServiceIds('fragment_handler') as $id => $attributes) {
      $services[] = $container->getDefinition($id);
    }

    foreach ($container->findTaggedServiceIds('render_strategy') as $id => $attributes) {
      foreach ($services as $service) {
        $service->addMethodCall('addRenderer', array(new Reference($id)));
      }
    }
  }
}
