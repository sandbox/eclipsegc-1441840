<?php

/**
 * @file
 * Contains \Drupal\Core\HtmlFragment.
 */

namespace Drupal\Core\Page;

use Drupal\Core\Asset\AssetBag;
use Drupal\Core\Asset\AssetBagInterface;

/**
 * Response object that contains variables for injection into the html template.
 *
 * @todo should we have this conform to an interface? https://drupal.org/node/1871596#comment-7134686
 * @todo add method replacements for *all* data sourced by html.tpl.php
 */
class HtmlFragment {

  /**
   * HTML content string.
   *
   * @var string
   */
  protected $content;

  /**
   * The title of this HtmlFragment.
   *
   * @var string
   */
  protected $title = '';

  public function __construct($content = '') {
    $this->content = $content;
    $this->bag = new AssetBag();
  }

  /**
   * Adds another asset bag to those already contained in this fragment.
   *
   * @param AssetBagInterface $bag
   */
  public function addAssetBag(AssetBagInterface $bag) {
    $this->bag->addAssetBag($bag);
  }

  /**
   * Returns the AssetBag representing the collected assets in this fragment.
   *
   * @return AssetBag
   */
  public function getAssets() {
    return $this->bag;
  }

  /**
   * Sets the response content.
   *
   * This should be the bulk of the page content, and will ultimately be placed
   * within the <body> tag in final HTML output.
   *
   * Valid types are strings, numbers, and objects that implement a __toString()
   * method.
   *
   * @param mixed $content
   *
   * @return \Drupal\Core\Page\HtmlFragment
   */
  public function setContent($content) {
    $this->content = $content;
    return $this;
  }

  /**
   * Gets the main content of this HtmlFragment.
   *
   * @return string
   */
  public function getContent() {
    return $this->content;
  }

  /**
   * Sets the title of this HtmlFragment.
   *
   * Handling of this title varies depending on what is consuming this
   * HtmlFragment object. If it's a block, it may only be used as the block's
   * title; if it's at the page level, it will be used in a number of places,
   * including the html <head> title.
   */
  public function setTitle($title, $output = CHECK_PLAIN) {
    $this->title = ($output == PASS_THROUGH) ? $title : check_plain($title);
  }

  /**
   * Indicates whether or not this HtmlFragment has a title.
   *
   * @return bool
   */
  public function hasTitle() {
    return !empty($this->title);
  }

  /**
   * Gets the title for this HtmlFragment, if any.
   *
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Clones the current HtmlFragment instance.
   */
  public function __clone() {
    $this->bag = clone $this->bag;
  }
}
