<?php
/**
 * @file
 * Contains Drupal\Core\Config\Entity\EmbeddedPluginConfigInterface.
 */

namespace Drupal\Core\Config\Entity;

/**
 * Interface for ConfigEntities that are used as embedded config by plugins.
 */
interface EmbeddedPluginConfigInterface extends ConfigEntityInterface {
  /**
   * Returns the id of the plugin in which this configuration is embedded.
   *
   * @return string
   */
  public function getPluginId();

  /**
   * Sets the id of the plugin in which this configuration is embedded.
   *
   * @param string $plugin_id
   *   The id of the plugin.
   *
   * @return void
   */
  public function setPluginId($plugin_id);
}