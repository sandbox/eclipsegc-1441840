<?php
/**
 * @file
 * Contains Drupal\Core\Booze\ContentPassthroughSubscriber.
 */

namespace Drupal\Core\Booze;

use Symfony\Cmf\Component\Routing\Enhancer\RouteEnhancerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Controller\ControllerResolver;

/**
 * Class for upcasting _content into a workable callback.
 *
 * We use the _content property on routes to simulate the 'page callback'
 * experience in previous versions of Drupal: it is the callback responsible
 * for populating the main content area of an HTML page. Because all such
 * top-level content must be encapsulated by blocks, this route enhancer
 * creates a closure with logic for calling it to be used later on.
 */
class ContentRouteEnhancer implements RouteEnhancerInterface {

  protected $resolver;

  public function __construct(ControllerResolver $resolver) {
    $this->resolver = $resolver;
  }

  public function enhance(array $defaults, Request $request) {
    // @todo this check is ridiculous, but temporarily necessary to narrow the scope to SCOTCH-y routes
    if (isset($defaults['_content']) && !isset($defaults['_content_closure']) && isset($defaults['_display'])) {
      // Clone the request and pretend like _content is the _controller
      $request = clone $request;
      $request->attributes->set('_controller', $defaults['_content']);

      $controller = $this->resolver->getController($request);
      $arguments = $this->resolver->getArguments($request, $controller);

      $defaults['_content_closure'] = function() use ($controller, $arguments) {
        return call_user_func_array($controller, $arguments);
      };
    }

    return $defaults;
  }
}
