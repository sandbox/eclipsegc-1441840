<?php
/**
 * @file
 * Contains Drupal\Core\Booze\DisplayRouteEnhancer.
 */

namespace Drupal\Core\Booze;

use Drupal\layout\Config\BoundDisplayInterface;
use Symfony\Cmf\Component\Routing\Enhancer\RouteEnhancerInterface;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;

class DisplayRouteEnhancer implements RouteEnhancerInterface {
  /**
   * Implements \Symfony\Cmf\Component\Routing\Enhancer\ŖouteEnhancerInterface.
   *
   * Upcasts _display identifiers on routes into their full Display objects.
   *
   * @param array $defaults
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return array
   *   The modified defaults. This enhancer will only operate on keys called
   *   "_display", and only if their value is a string.
   */
  public function enhance(array $defaults, Request $request) {
    if (isset($defaults['_display']) && is_string($defaults['_display'])) {

      $entity = entity_load('display', str_replace('display.bound.', '', $defaults['_display']));
      if (!$entity instanceof BoundDisplayInterface) {
        $route = $defaults[RouteObjectInterface::ROUTE_OBJECT];
        throw new NotFoundHttpException(sprintf("Display %s could not be found for route on path %s.", $defaults['_display'], $route->getPath()));
      }
      $defaults['_display'] = $entity;
    }

    return $defaults;
  }
}
