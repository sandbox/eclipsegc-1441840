<?php
/**
 * @file
 * Contains Drupal\html_test\TestControllers.
 */

namespace Drupal\html_test;

use Drupal\Core\Page\HtmlFragment;

/**
 * Controller for testing partial HTML responses.
 */
class TestController {
  public function testHtmlFragmentOutput() {
    return new HtmlFragment('<p>hello world</p>');
  }
}
