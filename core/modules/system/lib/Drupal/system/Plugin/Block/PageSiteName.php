<?php

/**
 * @file
 * Contains \Drupal\system\Plugin\Block\PageSiteName
 */

namespace Drupal\system\Plugin\Block;

use Drupal\block\BlockBase;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "page_site_name",
 *   admin_label = @Translation("Site Name"),
 *   module = "system",
 *   settings = {
 *     "status" = TRUE,
 *     "cache" = DRUPAL_NO_CACHE
 *   }
 * )
 */
class PageSiteName extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $name = filter_xss_admin(config('system.site')->get('name'));
    if ($name) {
      return array(
        '#children' => filter_xss_admin(config('system.site')->get('name')),
      );
    }
  }
}
