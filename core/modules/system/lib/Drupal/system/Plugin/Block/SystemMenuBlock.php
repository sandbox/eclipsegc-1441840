<?php

/**
 * @file
 * Contains \Drupal\system\Plugin\Block\SystemMenuBlock.
 */

namespace Drupal\system\Plugin\Block;

use Drupal\block\BlockBase;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Provides a 'System Menu' block.
 *
 * @Plugin(
 *   id = "system_menu_block",
 *   admin_label = @Translation("System Menu"),
 *   module = "system",
 *   derivative = "Drupal\system\Plugin\Derivative\SystemMenuBlock",
 *   context = {
 *     "url" = {
 *       "type" = "string"
 *     }
 *   }
 * )
 * )
 */
class SystemMenuBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function access() {
    // @todo The 'Tools' menu should be available to anonymous users.
    list($plugin, $derivative) = explode(':', $this->getPluginId());
    return ($GLOBALS['user']->uid || in_array($derivative, array(
      'menu-main',
      'menu-tools',
      'menu-footer')
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    list($plugin, $menu) = explode(':', $this->getPluginId());
    // Derivatives are prefixed with 'menu-'.
    $menu = substr($menu, 5);
    $url = '';

    try {
      $url = $this->getContextValue('url');
      menu_tree_set_path($menu, $url);
    }
    catch(PluginException $e) {}

    $ret = menu_tree($menu);

    if ($url) {
      // Undo the damage.
      $paths = drupal_static('menu_tree_set_path');
      unset($paths[$menu]);
    }

    return $ret;
  }

}
