<?php

namespace Drupal\system\Plugin\Block;

use Drupal\block\BlockBase;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "page_site_slogan",
 *   admin_label = @Translation("Site Slogan"),
 *   module = "system",
 *   settings = {
 *     "status" = TRUE,
 *     "cache" = DRUPAL_NO_CACHE
 *   }
 * )
 */
class PageSiteSlogan extends BlockBase {

  /**
   * Overrides \Drupal\block\BlockBase::blockBuild().
   */
  public function build() {
    $slogan = theme_get_setting('features.slogan');
    if ($slogan) {
      $slogan = config('system.site')->get('slogan');
      if (!empty($slogan)) {
        return array(
          '#children' => filter_xss_admin($slogan),
        );
      }
    }
  }
}
