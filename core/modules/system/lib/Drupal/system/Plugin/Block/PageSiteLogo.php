<?php

namespace Drupal\system\Plugin\Block;

use Drupal\block\BlockBase;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "page_site_logo",
 *   admin_label = @Translation("Site Logo"),
 *   module = "system",
 *   settings = {
 *     "status" = TRUE,
 *     "cache" = DRUPAL_NO_CACHE
 *   }
 * )
 */
class PageSiteLogo extends BlockBase {

  /**
   * Overrides \Drupal\block\BlockBase::blockBuild().
   */
  public function build() {
    $logo = theme_get_setting('logo');
    if (!empty($logo)) {
      $image = '<img src="' . $logo['url'] . '" alt="' . t('Home') . '" />';
      return array(
        '#children' => l($image, '', array('html' => TRUE, 'attributes' => array('rel' => 'home', 'id' => 'logo', 'title' => t('Home')))),
      );
    }
  }
}
