<?php

/**
 * @file
 * Contains \Drupal\system\Plugin\Block\SystemMainBlock.
 */

namespace Drupal\system\Plugin\Block;

use Drupal\block\BlockBase;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Provides a 'Main page content' block.
 *
 * @Plugin(
 *   id = "system_main_block",
 *   admin_label = @Translation("Main page content"),
 *   module = "system"
 * )
 */
class SystemMainBlock extends BlockBase {

  protected $controllerClosure;

  /**
   * {@inheritdoc}
   */
  public function build() {
    // @todo this lets the old way work transparently while we do new stuff.
    if ($ret = drupal_set_page_content()) {
      return array($ret);
    }

    return is_callable($this->controllerClosure) ? call_user_func($this->controllerClosure) : '';
  }

  /**
   * Sets the controller closure that returns the block content.
   *
   * @param \Closure $closure
   *   The closure/callable that provides the block content.
   */
  public function setControllerClosure(\Closure $closure) {
    $this->controllerClosure = $closure;
  }

}
