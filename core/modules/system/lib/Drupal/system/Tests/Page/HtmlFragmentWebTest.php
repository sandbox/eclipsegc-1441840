<?php
/**
 * @file
 * Contains \Drupal\system\Tests\Page\HtmlFragmentWebTest.
 */

namespace Drupal\system\Tests\Page;

use Drupal\simpletest\WebTestBase;
use Drupal\Core\Page\HtmlFragment;
use Drupal\Core\EventSubscriber\HtmlViewSubscriber;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernel;

class HtmlFragmentWebTest extends WebTestBase {

  public static $modules = array('html_test');

  public static function getInfo() {
    return array(
      'name' => 'HTML fragment response',
      'description' => 'Tests that HtmlFragment data is properly rendered into html.tpl.php.',
      'group' => 'Page',
    );
  }

  /**
   * Tests that a HtmlFragment object is rendered correctly by issuing an
   * HTTP request against it.
   */
  public function testHtmlFragmentOutputByWeb() {
    $this->drupalGet('html_test');

    // Check that our basic body text is there.
    $this->assertRaw('hello world');
    // Check that we didn't get a page within a page, inception-style.
    $this->assertNoPattern('#</body>.*</body>#s', 'There was no double-page effect from a misrendered subrequest.');
  }

  /**
   * Tests that a HtmlFragment object is rendered correctly by mocking the
   * environment.
   */
  public function testPartialResponseByUnit() {
    $kernel = $this->container->get('http_kernel');
    $request = Request::create('/foo');
    $response = new HtmlFragment('hello world');
    $subscriber = new HtmlViewSubscriber($this->container->get('module_handler'));
    $event = new GetResponseForControllerResultEvent($kernel, $request, HttpKernel::MASTER_REQUEST, $response);
    $subscriber->onHtmlFragmentResponse($event);
    $this->assertTrue(preg_match('/hello world/', $event->getResponse()->getContent()), 'Expected raw output found within HTML response.');
  }
}
