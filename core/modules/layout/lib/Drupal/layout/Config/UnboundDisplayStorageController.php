<?php
/**
 * @file
 * Contains Drupal\layout\Config\UnboundDisplayStorageController.
 */

namespace Drupal\layout\Config;

use Drupal\Core\Config\Entity\ConfigStorageController;
use Drupal\layout\Plugin\Core\Entity\Display;
use Drupal\Core\Config\Config;

/**
 * Storage controller for Unbound Displays.
 *
 * @see \Drupal\layout\Config\UnboundDisplayInterface
 */
class UnboundDisplayStorageController extends ConfigStorageController {
  public function importCreate($name, Config $new_config, Config $old_config) {
    // First, import the unbound display into active config
    $unbound_display = $this->create($new_config->get());
    $unbound_display->save();

    // Now, create a corresponding bound display - if one doesn't exist already.
    if (!entity_load('display', $name)) {
      // @todo need finer-grained control than a single global default
      // @todo how do we figure out whether we're doing frontend, admin, or...other?
      $layout = drupal_container()->get('plugin.manager.layout')
        ->createInstance(config('system.site')->get('default_layout'));
      $bound_display = $unbound_display->generateDisplay($layout, $name);
      $bound_display->save();
    }

    // @todo consider adding flexibility around which display type to create
//    $controller = drupal_container()
//      ->get('plugin.manager.entity')
//      ->getStorageController('display');

    // $controller->create();
    return TRUE;
  }
}
