<?php
/**
 * @file
 * Contains Drupal\layout\Config\HeirDisplayBase.
 */

namespace Drupal\layout\Config;

use Drupal\Component\Utility\NestedArray;

/**
 * Base class for Displays capable of receiving masters.
 */
abstract class HeirDisplayBase extends DisplayBase implements HeirDisplayInterface {
  /**
   * The string configuration key identifying this display's master, if any.
   *
   * @var string
   */
  public $masterId;

  /**
   * The entity type of the master display.
   *
   * Used to ensure we load the master display correctly via entity_load().
   *
   * @var string
   */
  public $masterType = 'display';

  /**
   * @var \Drupal\layout\Config\InheritableDisplayInterface
   */
  protected $master;

  /**
   * Contains inheritance-reconciled display data.
   *
   * Inheritance pulls down several properties from the master. This property
   * contains all the fully reconciled information, keyed by the name of the
   * object property that was merged.
   *
   * @var array
   */
  protected $reconciled = array();

  /**
   * Tracks whether or not master inheritance has been performed.
   *
   * @var bool
   */
  protected $inherited = FALSE;

  /**
   * {@inheritdoc}
   */
  public function getMaster() {
    if (!isset($this->master)) {
      if (empty($this->masterId)) {
        $this->master = FALSE;
      }
      else {
        $master = entity_load($this->masterType, $this->masterId);
        $this->master = $master instanceof InheritableDisplayInterface ? $master : FALSE;
      }
    }

    return $this->master;
  }

  /**
   * {@inheritdoc}
   */
  public function setMaster(InheritableDisplayInterface $display) {
    $this->flush();

    $this->masterType = $display->entityType();
    $this->masterId = $display->id();
    $this->master = $display;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllOuterBlockConfig() {
    $this->inherit();
    return $this->reconciled['blockInfo'];
  }

  /**
   * {@inheritdoc}
   */
  public function getAllEmbeddedBlockConfigs() {
    $this->inherit();
    return $this->reconciled['embeddedBlockConfigs'];
  }

  /**
   * Triggers inheritance from this display's master.
   *
   * Inheritance requires the reconciliation of multiple different properties.
   * This method reconciles everything at once, which allows central control of
   * inheritance state, rather than having to juggle it in each accessor method
   * separately.
   *
   * @return void
   */
  protected function inherit() {
    // Only calculate inherited properties if the state flag is not set.
    if ($this->inherited) {
      return;
    }
    $this->inherited = TRUE;

    $local_infos = $this->blockInfo;
    foreach ($local_infos as &$info) {
      // Indicate that this block is originally declared in this display.
      $info['owner'] = $this->id();
    }

    // Only try to inherit if there is a master.
    if (!$this->getMaster()) {
      // No master; merged block info will be identical to local block info.
      $this->reconciled['blockInfo'] = $local_infos;
      $this->reconciled['embeddedBlockConfigs'] = $this->embeddedBlockConfigs;
    }
    else {
      // This display is an heir with a master, so pull everything down.
      $master_infos = $this->getMaster()->getAllOuterBlockConfig();
      $master_embedded_configs = $this->getMaster()->getAllEmbeddedBlockConfigs();

      // @todo this needs to be thought through and made into a better pattern.
      // Allow local blocks to supersede masters.
      $this->reconciled['blockInfo'] = NestedArray::mergeDeep($master_infos, $local_infos);
      $this->reconciled['embeddedBlockConfigs'] = NestedArray::mergeDeep($master_embedded_configs, $this->getAllEmbeddedBlockConfigs());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function flush() {
    unset($this->reconciled, $this->master);
  }
}