<?php
/**
 * @file
 * Contains Drupal\layout\Config\InheritableDisplayInterface.
 */

namespace Drupal\layout\Config;

/**
 * Interface describing a display from which another display can inherit.
 *
 * Display inheritance is a mechanism by which one display can inherit blocks
 * defined in another. It is the primary mechanism by which site-wide block
 * configuration is accomplished: a single parent, or master, display can define
 * shared blocks, and then many "local" displays, each tied to a specific route,
 * can each inherit the shared blocks from that master, but also add its own.
 *
 * @see HeirDisplayInterface
 */
interface InheritableDisplayInterface extends DisplayInterface {
  // @todo get this to jive with the existing interface strategy.
}