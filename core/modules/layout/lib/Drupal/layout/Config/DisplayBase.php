<?php

/**
 * @file
 * Definition of Drupal\layout\Config\DisplayBase.
 */

namespace Drupal\layout\Config;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\block\Plugin\Type\BlockManager;
use Drupal\layout\Config\InheritableDisplayInterface;
use Drupal\layout\Config\HeirDisplayInterface;
use Drupal\layout\Plugin\LayoutInterface;

/**
 * Base class for 'display' and 'unbound_display' configuration entities.
 *
 * @see \Drupal\layout\Config\DisplayInterface
 */
abstract class DisplayBase extends ConfigEntityBase implements DisplayInterface {

  /**
   * The ID (config name) identifying a specific display object.
   *
   * @var string
   */
  public $id;

  /**
   * The UUID identifying a specific display object.
   *
   * @var string
   */
  public $uuid;

  /**
   * Contains all local block configuration.
   *
   * @todo update these docs to reflect the distinction between standalone and embedded blocks
   *
   * There are two levels to the configuration contained herein: display-level
   * block configuration, and then block instance configuration.
   *
   * Block instance configuration is stored in a separate config object. This
   * array is keyed by the config name that uniquely identifies each block
   * instance. At runtime, various object methods will retrieve this additional
   * config and return it to calling code.
   *
   * Display-level block configuration is data that determines the behavior of
   * a block *in this display*. The most important examples of this are the
   * region to which the block is assigned, and its weighting in that region.
   *
   * @code
   *    array(
   *      'block1-configkey' => array(
   *        'region' => 'content',
   *        // store the region type name here so that we can do type conversion w/out
   *        // needing to have access to the original layout plugin
   *        'region-type' => 'content',
   *        // increment by 100 so there is ALWAYS plenty of space for manual insertion
   *        'weight' => -100,
   *      ),
   *      'block2-configkey' => array(
   *        'region' => 'sidebar_first',
   *        'region-type' => 'aside',
   *        'weight' => -100,
   *      ),
   *      'block2-configkey' => array(
   *        'region' => 'sidebar_first',
   *        'region-type' => 'aside',
   *        'weight' => 0,
   *      ),
   *      'maincontent' => array(
   *        'region' => 'content',
   *        'region-type' => 'content',
   *        'weight' => -200,
   *      ),
   *    );
   * @endcode
   *
   * @var array
   */
  protected $blockInfo = array();

  /**
   * Contains block configuration for all embedded blocks.
   *
   * This is an associative array, keyed on internal block id, with each value
   * being an array of the internal configuration for a block that is embedded
   * directly on this display. This is the counterpart to standalone storage via
   * an independent configuration object - exactly the same data is stored, but
   * it is located directly on the display rather than in a separate object.
   *
   * @see \Drupal\block\Plugin\Core\Entity\Block
   *
   * @var array
   */
  protected $embeddedBlockConfigs = array();

  /**
   * An associative array with information about the groups contained in this
   * display.
   *
   * @var array
   */
  protected $groups;

  /**
   * @var \Drupal\block\Plugin\Type\BlockManager
   */
  protected $blockManager;

  /**
   * An associated array of context configuration.
   *
   * @var array
   */
  protected $contexts = array();

  /**
   * {@inheritdoc}
   */
  public function getContextConfigurations() {
    return $this->contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function getContextConfiguration($id) {
    if (isset($this->contexts[$id])) {
      return $this->contexts[$id];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setContextConfiguration($id, $value) {
    $this->contexts[$id] = $value;
  }

  /**
   * {@inheritdoc}
   *
   * @todo this makes unserialization of displays hard - though that may not be an issue.
   */
  public function setBlockManager(BlockManager $blockManager) {
    $this->blockManager = $blockManager;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockManager() {
    if (!isset($this->blockManager)) {
      // @todo reaching out like this sucks, but injecting into entities via their storage controllers would require mucking with the EntityManager...not worth it yet.
      $this->blockManager = \Drupal::service('plugin.manager.block');
    }
    return $this->blockManager;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \InvalidArgumentException
   */
  public function getBlock($id) {
    $infos = $this->getAllOuterBlockConfig();
    $configs = $this->getAllEmbeddedBlockConfigs();

    if (!isset($infos[$id])) {
      throw new \InvalidArgumentException(sprintf("Display '%s' does not contain a block with id '%s'.", $this->id(), $id));
    }

    // Figure out if we're dealing with an embedded or standalone block.
    if (empty($infos[$id]['embedded'])) {
      // The block is standalone; have the BlockManager load it by config_id.
      return $this->getBlockManager()->getInstanceViaConfig($id);
    }
    else {
      $config = $configs[$id];
      $plugin_id = $config['plugin'];
      return $this->getBlockManager()->createInstance($plugin_id, $config['settings']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getAllEmbeddedBlockConfigs() {
    return $this->embeddedBlockConfigs;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllGroupConfig() {
    return $this->groups;
  }

  /**
   * {@inheritdoc}
   *
   * @todo Decouple this implementation from this class and place it onto the layout plugin.
   */
  public function mapBlocksToLayout(LayoutInterface $layout) {
    $types = array();

    $layout_regions = $layout->getRegions();
    $layout_regions_indexed = array_keys($layout_regions);
    foreach ($layout_regions as $name => $info) {
      $types[$info['type']][] = $name;
    }

    $remapped_config = array();
    foreach ($this->blockInfo as $name => $info) {
      // First, if there's a direct region name match, use that.
      if (!empty($info['region']) && isset($layout_regions[$info['region']])) {
        // No need to do anything.
      }
      // Then, try to remap using region types.
      else if (!empty($types[$info['region-type']])) {
        $info['region'] = reset($types[$info['region-type']]);
      }
      // Finally, fall back to dumping everything in the layout's first region.
      else {
        if (!isset($first_region)) {
          reset($layout_regions);
          $first_region = key($layout_regions);
        }
        $info['region'] = $first_region;
      }

      $remapped_config[$name] = $info;
    }

    return $remapped_config;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllRegionTypes() {
    $types = array();
    foreach ($this->blockInfo as $info) {
      $types[] = $info['region-type'];
    }
    return array_unique($types);
  }

  /**
   * {@inheritdoc}
   */
  public function getExportProperties() {
    $properties = parent::getExportProperties();
    $properties['blockInfo'] = $this->blockInfo;
    $properties['embeddedBlockConfigs'] = $this->embeddedBlockConfigs;
    $properties['groups'] = $this->groups;
    $properties['contexts'] = $this->contexts;
    return $properties;
  }
}
