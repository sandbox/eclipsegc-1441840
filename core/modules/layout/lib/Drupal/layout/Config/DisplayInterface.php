<?php
/**
 * @file
 * Definition of Drupal\layout\Config\DisplayInterface
 */

namespace Drupal\layout\Config;

use Drupal\Core\Entity\EntityInterface;
use Drupal\block\BlockInterface;
use Drupal\block\Plugin\Type\BlockManager;
use Drupal\layout\Plugin\LayoutInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface describing a Display configuration object.
 *
 * Displays are configuration that describe the placement of block instances
 * in regions. Drupal includes two types of Display objects:
 * - Bound displays include a reference to a specific layout, and each block is
 *   specified to display in a specific region of that layout. Bound displays
 *   are used to serve real pages at request time.
 * - Unbound displays do not include a reference to any layout, and each block
 *   is assigned a region type, but not a specific region. Developers including
 *   default displays with their modules or distributions are encouraged to use
 *   unbound displays in order to minimize dependencies on specific layouts and
 *   allow site-specific configuration to dictate the layout.
 *
 * This interface defines what is common to all displays, whether bound or
 * unbound.
 *
 * @see \Drupal\layout\Config\BoundDisplayInterface
 * @see \Drupal\layout\Config\UnboundDisplayInterface
 */
interface DisplayInterface extends ConfigEntityInterface {

  /**
   * Returns the display-specific configuration of all blocks in this display.
   *
   * @todo update docs to reflect separation between 'embedded' and 'standalone' blocks
   *
   * For each block that exists in Drupal (e.g., the "Who's Online" block),
   * multiple "configured instances" can be created (e.g., a "Who's been online
   * in the last 5 minutes" instance and a "Who's been online in the last 60
   * minutes" instance). Each configured instance can be referenced by multiple
   * displays (e.g., by a "regular" page, by an administrative page, and within
   * one or more dashboards). This function returns the block instances that
   * have been added to this display. Each key of the returned array is the
   * block instance's configuration object name, and config() may be called on
   * it in order to retrieve the full configuration that is shared across all
   * displays. For each key, the value is an array of display-specific
   * configuration, primarily the 'region' and 'weight', and anything else that
   * affects the placement of the block within the layout rather than only the
   * contents of the block.
   *
   * @return array
   *   An array keyed on each block's configuration object name. Each value is
   *   an array of information that determines the placement of the block within
   *   a layout, including:
   *   - region: The region in which to display the block (for bound displays
   *     only).
   *   - region-type: The type of region that is most appropriate for the block.
   *     Usually one of 'header', 'footer', 'nav', 'content', 'aside', or
   *     'system', though custom region types are also allowed. This is
   *     primarily specified by unbound displays, where specifying a specific
   *     region name is impossible, because different layouts come with
   *     different regions.
   *   - weight: Within a region, blocks are rendered from low to high weight.
   */
  public function getAllOuterBlockConfig();

  /**
   * Returns all embedded block configurations.
   *
   * @return array
   */
  public function getAllEmbeddedBlockConfigs();

  /**
   * Return the configuration for all region groups in this display.
   *
   * @return array
   */
  public function getAllGroupConfig();

  /**
   * Gets a configured BlockInterface instance.
   *
   * This method abstracts the difference between a reusable/externally
   * configured block and a block that is embedded within the display.
   *
   * @todo this separation of responsibilities is still a little awkward.
   *
   * @param $id
   *   The display's internal id for the block.
   *
   * @return \Drupal\block\BlockInterface
   */
  public function getBlock($id);

  /**
   * Injects a BlockManager.
   *
   * Displays use the BlockManager to create BlockInterface objects with their
   * configuration already correctly injected.
   *
   * @todo this will eventually be tricky because of the difficulties the BlockManager may have with loading different block entity types (custom block vs. generic reusable block)
   * @todo this makes unserialization of displays hard - though that may not be a concern.
   *
   * @param BlockManager $blockManager
   *
   * @return DisplayInterface
   *   The current DisplayInterface object.
   */
  public function setBlockManager(BlockManager $blockManager);

  /**
   * Returns the BlockManager contained in this display.
   *
   * @return BlockManager
   */
  public function getBlockManager();

  /**
   * Maps the contained block info to the provided layout.
   *
   * @param \Drupal\layout\Plugin\LayoutInterface $layout
   *
   * @return array
   *   An array containing block configuration info, identical to that which
   *   is returned by DisplayInterface::getAllOuterBlockConfig().
   */
  public function mapBlocksToLayout(LayoutInterface $layout);

  /**
   * Returns the names of all region types to which blocks are assigned.
   *
   * @return array
   *   An indexed array of unique region type names, or an empty array if no
   *   region types were assigned.
   */
  public function getAllRegionTypes();

  /**
   * Flushes all internally calculated and cached data from the display.
   *
   * Displays load and/or calculate, then attach a lot of data as local object
   * properties: the layout plugin instance, their master display (if an heir),
   * and potentially also block object instances. This method flushes all that
   * locally cached data, which may be necessary if some of the raw data
   * underlying some of those calculated properties changes (e.g., a new master
   * is swapped in).
   *
   * @return void
   */
  public function flush();

  /**
   * Gets the configured contexts.
   *
   * @return array
   *   The configuration of all the available contexts.
   */
  public function getContextConfigurations();

  /**
   * Get a specific configured context.
   *
   * @param $id
   *   The array key for the desired context.
   *
   * @return array
   *   The configuration for the desired context.
   */
  public function getContextConfiguration($id);

  /**
   * Set the configuration of a specific context.
   *
   * @param $id
   *   The array key of the context element to set.
   * @param $value
   *   The value to set for the context element.
   */
  public function setContextConfiguration($id, $value);
}
