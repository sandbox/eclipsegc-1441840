<?php
/**
 * @file
 * Contains \Drupal\layout\Config\HeirDisplayInterface.
 */
namespace Drupal\layout\Config;

/**
 * Interface describing a display that can inherit from another display.
 *
 * @see InheritableDisplayInterface
 */
interface HeirDisplayInterface extends DisplayInterface {

  /**
   * Sets the master display from which this display should inherit.
   *
   * @param InheritableDisplayInterface $display
   *   The master display from which this display should inherit.
   *
   * @return HeirDisplayInterface
   *   The current HeirDisplayInterface object.
   */
  public function setMaster(InheritableDisplayInterface $display);

  /**
   * Returns the master display from which this display inherits, if any.
   *
   * @return bool|\Drupal\layout\Config\InheritableDisplayInterface
   *   This display's master, or FALSE if there is no master.
   */
  public function getMaster();
}