<?php
/**
 * @file
 * Contains Drupal\layout\Config\UnboundDisplayStorageController.
 */

namespace Drupal\layout\Config;

use Drupal\Core\Config\Entity\ConfigStorageController;
use Drupal\block\Plugin\Type\BlockManager;
use Drupal\layout\Config\DisplayInterface;

/**
 * Storage controller for Displays.
 *
 * @see \Drupal\layout\Config\BoundDisplayInterface
 */
class DisplayStorageController extends ConfigStorageController {
  /**
   * @param array $values
   *   An array of values with which to create the new Display object.
   *
   * @return \Drupal\layout\Config\DisplayInterface
   */
  public function create(array $values) {
    $display = parent::create($values);
    if ($display instanceof HeirDisplayInterface && !empty($values['masterId'])) {
      entity_load($values['masterType'], $values['masterId']);
    }
    // @todo ideally we should inject the BlockManager into displays here...but it's already hard to get the service into this storage controller.
  }

}
