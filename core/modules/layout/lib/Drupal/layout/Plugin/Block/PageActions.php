<?php

namespace Drupal\layout\Plugin\Block;

use Drupal\block\BlockBase;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "page_actions",
 *   subject = @Translation("Menu Actions"),
 *   module = "layout",
 *   settings = {
 *     "status" = TRUE,
 *     "cache" = DRUPAL_NO_CACHE
 *   }
 * )
 */
class PageActions extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      menu_get_local_actions()
    );
  }
}
