<?php
/**
 * @file
 * Contains Drupal\block\BlockAddressGenerator.
 */

namespace Drupal\block;

use Drupal\Block\BlockInterface;
use Drupal\layout\Config\BoundDisplayInterface;
use Drupal\block\Exception\MissingRequiredDataException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Generates addressable URLs for blocks.
 */
class BlockAddressGenerator {

  /**
   * @var \Symfony\Component\Routing\Generator\UrlGeneratorInterface
   */
  protected $generator;

  /**
   * A static map containing the known addressable block routes.
   *
   * @var array
   */
  protected $routes = array(
    'embedded' => 'block_address_embedded',
    'standalone' => 'block_address_standalone',
    'main' => 'block_address_main',
  );

  public function __construct(UrlGeneratorInterface $generator) {
    $this->generator = $generator;
  }

  /**
   * Generates a URL for the injected block.
   *
   * The block must have its configuration injected, and all of its context
   * requirements satisfied before being passed to this method.
   *
   * @param \Drupal\Block\BlockInterface $block
   *   The block for which a url should be generated.
   *
   * @param \Drupal\layout\Config\BoundDisplayInterface $display
   *   Optional. Providing the display indicates that we are generating a route
   *   to a block embedded within that display.
   *
   * @return string
   *   The generated URL
   *
   * @throws \Drupal\block\Exception\MissingRequiredDataException
   */
  public function generateAddress(BlockInterface $block, Request $request, BoundDisplayInterface $display = NULL) {
    if (!$block->contextIsSatisfied()) {
      throw new MissingRequiredDataException(sprintf("Cannot generate an addressable URL for block '%s' as not all of its context has been injected.", $block->getPluginId()));
    }

    $params = array();
    foreach ($block->getContexts() as $name => $context) {
      // @todo this is pseudocode, the underlying systems here are not yet ready...i think
      $params[$name] = $context->getType() . ':' . $context->getId();
    }
    // @todo missing the route, how should we get that in here...pass in the request object? kinda eew.
    // @todo handle the main content block. UGH.

    // Add different parameters if the block is standalone or embedded; if a
    // display was passed in, that means it's embedded.
    if (!empty($display)) {
      $params['display'] = $display->id(); // @todo this is sans-prefix, we need with-prefix
      $route = $this->routes['embedded'];
    }
    else {
      $params['route'] = $request->attributes->get('_route');
      $route = $this->routes['standalone'];
    }

    $params['block_id'] = $block->getInstanceId();
    return $this->generator->generate($route, $params);
  }
}