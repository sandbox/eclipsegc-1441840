<?php

/**
 * Contains \Drupal\block\Plugin\Type\BlockManager.
 */

namespace Drupal\block\Plugin\Type;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Mapper\ConfigDrivenMapperInterface;
use Drupal\block\BlockInterface;
use Drupal\block\Plugin\Core\Entity\Block;
/**
 * Manages discovery and instantiation of block plugins.
 *
 * @todo Add documentation to this class.
 *
 * @see \Drupal\block\BlockPluginInterface
 */
class BlockManager extends DefaultPluginManager implements ConfigDrivenMapperInterface {

  protected $defaults = array(
    'semantics' => 'div',
    'has_native_access_logic' => TRUE,
    // Indicates if a block can safely be rendered by address-based strategies.
    'safely_addressable' => FALSE, // @todo it'd be great to default to TRUE here.
  );

  /**
   * Constructs a new \Drupal\block\Plugin\Type\BlockManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The language manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, LanguageManager $language_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct('Block', $namespaces);
    $this->alterInfo($module_handler, 'block');
    $this->setCacheBackend($cache_backend, $language_manager, 'block_plugins');
  }

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\block\BlockInterface
   *   A configured BlockInterface instance.
   */
  public function getInstanceViaConfig($config_id, $plugin_id = '') {
    // Allow for different prefixes - e.g., custom vs. generic reusable blocks.
    $entity = config_load_entity_by_name($config_id);

    if (!$entity instanceof BlockInterface) {
      throw new PluginException(sprintf('No block configuration found for config id "%s".', $config_id));
    }

    $plugin_id = $plugin_id ?: $entity->getPluginId();
    return $this->createInstance($plugin_id, $entity->get('settings'));
  }

}
