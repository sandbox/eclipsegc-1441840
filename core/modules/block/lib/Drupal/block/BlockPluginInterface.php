<?php

/**
 * @file
 * Contains \Drupal\block\BlockPluginInterface.
 */

namespace Drupal\block;

use Drupal\Component\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Asset\AssetCollector;

/**
 * Defines the required interface for all block plugins.
 *
 * @todo Add detailed documentation here explaining the block system's
 *   architecture and the relationships between the various objects, including
 *   brif references to the important components that are not coupled to the
 *   interface.
 *
 * @see \Drupal\block\BlockBase
 */
interface BlockPluginInterface extends ContextAwarePluginInterface {

  /**
   * Returns the default settings for this block plugin.
   *
   * @return array
   *   An associative array of block settings for this block, keyed by the
   *   setting name.
   *
   * @todo Consider merging this with the general plugin configuration member
   *   variable and its getter/setter in http://drupal.org/node/1764380.
   */
  public function settings();

  /**
   * Indicates whether the block should be shown.
   *
   * This method allows base implementations to add general access restrictions
   * that should apply to all extending block plugins.
   *
   * @return bool
   *   TRUE if the block should be shown, or FALSE otherwise.
   *
   * @see \Drupal\block\BlockAccessController
   */
  public function access();

  /**
   * Constructs the block configuration form.
   *
   * This method allows base implementations to add a generic configuration
   * form for extending block plugins.
   *
   * @param array $form
   *   The form definition array for the block configuration form.
   * @param array $form_state
   *   An array containing the current state of the configuration form.
   *
   * @return array $form
   *   The renderable form array representing the entire configuration form.
   *
   * @see \Drupal\block\BlockFormController::form()
   * @see \Drupal\block\BlockInterace::validate()
   * @see \Drupal\block\BlockInterace::submit()
   */
  public function form($form, &$form_state);

  /**
   * Handles form validation for the block configuration form.
   *
   * @param array $form
   *   The form definition array for the block configuration form.
   * @param array $form_state
   *   An array containing the current state of the configuration form.
   *
   * @see \Drupal\block\BlockFormController::validate()
   * @see \Drupal\block\BlockInterace::form()
   * @see \Drupal\block\BlockInterace::submit()
   */
  public function validate($form, &$form_state);

  /**
   * Handles form submissions for the block configuration form.
   *
   * @param array $form
   *   The form definition array for the block configuration form.
   * @param array $form_state
   *   An array containing the current state of the configuration form.
   *
   * @see \Drupal\block\BlockFormController::submit()
   * @see \Drupal\block\BlockInterace::form()
   * @see \Drupal\block\BlockInterace::validate()
   */
  public function submit($form, &$form_state);

  /**
   * Builds and returns the renderable array for this block plugin.
   *
   * @return array
   *   A renderable array representing the content of the block.
   *
   * @see \Drupal\block\BlockRenderController
   */
  public function build();

  /**
   * Indicates whether or not this block can be safely addressed.
   *
   * "Addressable" refers to any rendering strategy where the block's rendering
   * process is arrived at via an addressing route, either for direct rendering
   * (i.e., via a subrequest) or via a deferred strategy that relies on an
   * external agent (e.g., ESI or hInclude).
   *
   * @return bool
   */
  public function isSafelyAddressable();

  /**
   * Declares the assets required by this block to a collector.
   *
   * @param \Drupal\Core\Asset\AssetCollector $collector
   *
   * @return void
   */
  public function declareAssets(AssetCollector $collector);

  /**
   * Returns the HTML tag that should be used to wrap this block.
   *
   * NOTE: Drupal 8 core does not currently use or respect these values.
   *
   * @return string
   */
  public function getSemantics();

  /**
   * Gets the default, unprocessed, frontend-facing title for this block.
   *
   * This title should be the block's suggestion of a sane title for frontend
   * display to the end user. If this block's title changes based on injected
   * contextual data, then this string should contain the relevant tokens.
   *
   * It must be possible to produce this title with neither configuration nor
   * contextual data having been injected into the block.
   *
   * The block is responsible for filtering this string appropriately if it
   * could contain user input.
   *
   * @return string
   */
  public function getDefaultTitle();

  /**
   * Gets the unprocessed user-facing title for this block.
   *
   * This method should allow user configuration to modify or fully replace the
   * suggested title returned from BlockPluginInterface::getDefaultTitle()
   * based on user configuration. As with that default title, returning a string
   * with raw tokens in it is appropriate.
   *
   * If the relevant configuration is not present, or if the Block chooses not
   * to allow any user modification of the title output, this method should
   * simply defer to BlockPluginInterface::getDefaultTitle().
   *
   * This string may contain user input, and should be filtered accordingly.
   *
   * @return string
   *
   * @see \Drupal\Block\BlockPluginInterface::getDefaultTitle()
   */
  public function getRawTitle();

  /**
   * Gets the fully-prepared user-facing title for this block.
   *
   * In the simple case, this method should take the output of
   * BlockPluginInterface::getRawTitle(), apply any transformational logic to it
   * that requires injected contextual data, and return the result.
   *
   * This method should not be called unless a block has had both configuration
   * and all necessary contexts properly injected.
   *
   * This string may contain user input, and should be filtered accordingly.
   *
   * @return string
   *
   * @see \Drupal\Block\BlockPluginInterface::getDefaultTitle()
   * @see \Drupal\Block\BlockPluginInterface::getRawTitle()
   */
  public function getTitle();

  /**
   * Indicates whether the block title should be shown on the frontend.
   *
   * @return bool
   */
  public function isTitleVisible();
}
