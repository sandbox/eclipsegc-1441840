<?php
/**
 * @file
 * Contains Drupal\block\DefaultBlockController.
 */

namespace Drupal\block;

use Drupal\Core\Asset\AssetBag;
use Drupal\Core\Asset\AssetCollector;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Page\HtmlFragment;
use Drupal\block\BlockPluginInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Default controller for rendering a block.
 */
class DefaultBlockController implements BlockControllerInterface {

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  public function __construct(ModuleHandlerInterface $moduleHandler) {
    $this->moduleHandler = $moduleHandler;
  }
  /**
   * Renders a provided block into an HtmlFragment.
   *
   * @param Request        $request
   * @param BlockPluginInterface $block
   *
   * @return \Drupal\Core\Page\HtmlFragment
   */
  public function respond(Request $request, BlockPluginInterface $block) {
    $fragment = new HtmlFragment();

    if ($build = $block->build()) {
      $render = array(
        '#theme' => 'block_ng',
        '#block' => $block,
        'content' => $build,
        '#configuration' => $block->getConfig(),
        '#label' => check_plain($block->getTitle()),
      );

      $this->moduleHandler->alter('block_ng_view', $render);

      $collector = new AssetCollector();
      $bag = new AssetBag();
      $collector->setBag($bag);
      $block->declareAssets($collector);

      $fragment->addAssetBag($bag);
      $fragment->setContent(drupal_render($render));
      $fragment->setTitle($block->getTitle());
    }

    return $fragment;
  }
}