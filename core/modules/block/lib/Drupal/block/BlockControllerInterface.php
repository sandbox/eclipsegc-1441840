<?php
/**
 * @file
 * Contains Drupal\block\BlockControllerInterface.
 */

namespace Drupal\block;

use Drupal\Core\Page\HtmlFragment;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface describing block controllers.
 *
 * Block controllers are responsible for injecting incoming context data onto a
 * block controller and making an HtmlFragment response out of its built render
 * array.
 */
interface BlockControllerInterface {
  /**
   * Renders a provided block into an HtmlFragment.
   *
   * @param Request        $request
   * @param BlockPluginInterface $block
   *
   * @return \Drupal\Core\Page\HtmlFragment
   */
  public function respond(Request $request, BlockPluginInterface $block);
}