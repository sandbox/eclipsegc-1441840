<?php
/**
 * @file
 * Contains Drupal\block\AddressedBlockAccessCheck.
 */

namespace Drupal\block;

use Drupal\Core\Access\AccessCheckInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

/**
 * Access check for addressable block routes.
 */
class AddressedBlockAccessCheck implements AccessCheckInterface {
  public function applies(Route $route) {
    return array_key_exists('_access_addressed_block', $route->getRequirements());
  }

  /**
   * Checks for access to route.
   *
   * @param \Symfony\Component\Routing\Route          $route
   *   The route to check against.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return mixed
   *   TRUE if access is allowed.
   *   FALSE if not.
   *   NULL if no opinion.
   */
  public function access(Route $route, Request $request) {
    $block = $request->attributes->get('_block');
    return $block->access();
  }
}