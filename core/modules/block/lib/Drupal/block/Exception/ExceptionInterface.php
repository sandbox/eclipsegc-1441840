<?php
/**
 * @file
 * Contains Drupal\block\ExceptionInterface.
 */

namespace Drupal\block\Exception;

/**
 * A general exception interface for all block-related exceptions.
 */
interface ExceptionInterface {

}