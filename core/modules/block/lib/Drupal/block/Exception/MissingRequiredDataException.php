<?php
/**
 * @file
 * Contains Drupal\block\MissingRequiredDataException.
 */

namespace Drupal\block\Exception;

use InvalidArgumentException;
use Drupal\block\Exception\ExceptionInterface;

/**
 * Exception indicating that a block is missing certain injected data.
 *
 * Throw this exception if an operation is attempted on or by a block that
 * requires that block to have all of its contextual data and configuration
 * requirements met.
 */
class MissingRequiredDataException extends InvalidArgumentException implements ExceptionInterface {}