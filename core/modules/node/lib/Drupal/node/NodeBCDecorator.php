<?php

/**
 * @file
 * Contains \Drupal\node\NodeBCDecorator.
 */

namespace Drupal\node;

use Drupal\Core\Entity\EntityBCDecorator;

/**
 * Defines the node specific entity BC decorator.
 */
class NodeBCDecorator extends EntityBCDecorator implements NodeInterface {
}
