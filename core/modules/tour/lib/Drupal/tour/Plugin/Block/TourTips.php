<?php

/**
 * @file
 * Contains \Drupal\tour\Plugin\Block\TourTips.
 */

namespace Drupal\tour\Plugin\Block;

use Drupal\Core\Asset\AssetCollector;
use Drupal\block\BlockBase;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Asset\AssetBag;
use Drupal\Core\Asset\StylesheetFileAsset;
use Drupal\Core\Asset\JavascriptFileAsset;
use Drupal\Core\Annotation\Translation;

/**
 * Provides a tour tips block.
 *
 * @Plugin(
 *   id = "tour_tips",
 *   admin_label = @Translation("Tour tips"),
 *   has_native_access_logic = FALSE,
 *   module = "tour"
 * )
 */
class TourTips extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function declareAssets(AssetCollector $collector) {
    // CSS Assets.
    $collector->createStylesheetFileAsset(DRUPAL_ROOT . '/core/modules/tour/css/joyride-2.0.3.css');
    $collector->createStylesheetFileAsset(DRUPAL_ROOT . '/core/modules/tour/css/tour-rtl.css');
    $collector->createStylesheetFileAsset(DRUPAL_ROOT . '/core/modules/tour/css/tour.css');

    // JS Assets.
    $collector->createJavascriptFileAsset(DRUPAL_ROOT . '/core/modules/tour/js/jquery.joyride-2.0.3.js');
    $collector->createJavascriptFileAsset(DRUPAL_ROOT . '/core/modules/tour/js/tour.js');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (!user_access('access tour')) {
      return;
    }

    // @todo replace this with http://drupal.org/node/1918768 once it is committed.
    $path = current_path();
    $tour_items = array();
    // Load all of the items and match on path.
    $tours = entity_load_multiple('tour');

    $path_alias = drupal_strtolower(drupal_container()->get('path.alias_manager')->getPathAlias($path));
    foreach ($tours as $tour_id => $tour) {
      // @todo Replace this with an entity query that does path matching when
      // http://drupal.org/node/1918768 lands.
      $pages = implode("\n", $tour->getPaths());
      if (!drupal_match_path($path_alias, $pages) && (($path == $path_alias) || drupal_match_path($path, $pages))) {
        unset($tours[$tour_id]);
      }
    }

    if ($tours) {
      return entity_view_multiple($tours, 'full');
    }
  }

}
