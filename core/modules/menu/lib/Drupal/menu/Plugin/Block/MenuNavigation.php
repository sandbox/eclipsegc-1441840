<?php

/**
 * @file
 * Contains \Drupal\menu\Plugin\Block\MenuNavigation.
 */

namespace Drupal\menu\Plugin\Block;

use Drupal\block\BlockBase;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Provides a navigation block for displaying menu links of a specified depth.
 *
 * @Plugin(
 *   id = "menu_navigation",
 *   admin_label = @Translation("Menu navigation"),
 *   module = "menu"
 * )
 */
class MenuNavigation extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function settings() {
    return array(
      'menu' => 'main',
      'level' => 0,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, &$form_state) {
    $form['menu'] = array(
      '#type' => 'select',
      '#title' => t('Source menu'),
      '#default_value' => $this->configuration['menu'],
      '#options' => menu_get_menus(),
      '#required' => TRUE,
    );
    $form['level'] = array(
      '#type' => 'select',
      '#title' => t('Starting level'),
      '#default_value' => $this->configuration['level'],
      '#options' => drupal_map_assoc(array(0, 1, 2, 3, 4, 5, 6, 7, 8)),
      '#required' => TRUE,
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, &$form_state) {
    $this->setConfig('menu', $form_state['values']['menu']);
    $this->setConfig('level', $form_state['values']['level']);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // @todo BlockManager fails to merge default settings.
    $this->configuration += $this->settings();

    $menu_name = $this->configuration['menu'];
    $level = $this->configuration['level'];
    // @todo menu_nagivation_links should be migrated to a menu link service and
    // that should be injected here.
    $links = menu_navigation_links($menu_name, $level);
    // Do not output this entire block if there are no visible/accessible links.
    // Filter the links to remove those with #access set to FALSE.
    array_filter($links, function($link) {
      if (isset($link['#access']) && $link['#access'] === FALSE) {
        return FALSE;
      }
      return TRUE;
    });
    if (empty($links)) {
      return array();
    }
    $build = array(
      '#theme' => 'links',
      '#links' => $links,
    );
    if (!empty($this->configuration['id'])) {
      $build['#attributes']['id'] = drupal_html_id($this->configuration['id']);
    }
    return $build;
  }

}
