<?php

/**
 * @file
 * Contains \Drupal\menu\Plugin\Block\MenuBlock.
 */

namespace Drupal\menu\Plugin\Block;

use Drupal\system\Plugin\Block\SystemMenuBlock;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Provides a generic Menu block.
 *
 * @Plugin(
 *   id = "menu_menu_block",
 *   admin_label = @Translation("Menu"),
 *   module = "menu",
 *   derivative = "Drupal\menu\Plugin\Derivative\MenuBlock",
 *   context = {
 *     "url" = {
 *       "type" = "string"
 *     }
 *   }
 * )
 */
class MenuBlock extends SystemMenuBlock {}
